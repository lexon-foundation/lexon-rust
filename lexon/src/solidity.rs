use crate::ast::*;
use crate::vm::*;

pub fn type2str(_tt:&Type)->String{
    //println!("_tt: {:?}",_tt );
    let t=match _tt{
        Type::Amount=>"uint", 
        Type::Person=>"address payable",
        Type::Key=>"address",
        Type::Data=>"bytes32",
        Type::Time=>"uint",
        Type::Text=>"string",
        Type::Binary=>"bool",
        Type::Asset=>"bytes32",
        Type::Contract=>"",//special case to get this contract
        _=>"//err"
    };
    return t.to_string()
}

pub fn sol_operand(op:&Operand,variables:&Vec<Variable>,params:&Vec<Variable>)-> String {
    match op{
        Operand::Val(v)=>{
            match v{
                Value::Id(i)=>{
                    i.to_string()
                },
                Value::Date(d)=>{
                    d.to_string()
                },
                Value::Binary(b)=>{
                    match b{
                        Some(v)=>v.to_string(),
                        None=> "false".to_string()
                    }
                },
                Value::Integer(i)=>{
                    i.to_string()
                },
                Value::ThisAddress=>{
                    "address(this)".to_string()
                },
                Value::MsgValue=>"msg.value".to_string()
            }
        },
        Operand::Var(v)  => {
            //if v=="escrow" {
            //    "address(this)".to_string()
            //}else{
                v.to_string()
           // }
        },
        Operand::Par(p)  => p.to_string(),
        Operand::Exp(e)  => sol_expr(&e,variables,params),
        Operand::Func(f) => f.to_string()
    }
}

pub fn sol_operations(ops:&Vec<Operation>,sol:&mut Vec<String>,variables:&Vec<Variable>,params:&Vec<Variable>,mays:usize){
    //let mut ret="".to_string();
    let mut ret_list:Vec<String>=Vec::new();
    let mut may=0;
    for op in ops{
        match op{
            Operation::Transfer{who,from: _,exp,to}=>{
                let to=sol_operand(to ,variables,params);
                let  e=sol_operand(exp,variables,params);
                match who{
                    Some(_)=>{ // w
                        //println!("exp {:#?}",e );
                        //let w=sol_operand(w,variables);
                        //sol.push(format!("//{} {}.transfer({});",w,to,e))
                        //sol.push(format!("{}.transfer({});",to,e))
                        sol.push(format!("//{}.transfer({});",to,e))
                    },
                    None => sol.push(format!("{}.transfer({});",to,e))
                };
            },
            Operation::Assign(op1,op2)=>{
                let dest=sol_operand(op1,variables,params);
                let what=sol_operand(op2,variables,params);
                sol.push(format!("{}={};",dest,what));  
            },
            Operation::Reveal(list)=>{
                ret_list=list.into_iter().map(|op|
                    sol_operand(op,variables,params)
                ).collect();
            },
            Operation::Terminate=>{
                sol.push(format!("selfdestruct(address(0));"));
            },
            Operation::May(op_who,ops)=>{
                let mut tmp:Vec<String>=Vec::new();
                
                let who=sol_operand(op_who,variables,params);
                
                sol.push(format!("{}if (msg.sender == {}){{",
                    if may==0 {""}else{"}else "},
                    who));
                
                sol_operations(ops,sol,variables,params,0);
                
                tmp.push(format!("}}")); 
                if mays == 0 {
                    tmp.push(format!("else{{")); 
                    sol.push(tmp.join(""));
                    sol.push(format!("require(false);")); 
                    sol.push(format!("}}"));
                }else{
                    if may==mays-1 {
                        tmp.push(format!("else{{")); 
                        sol.push(tmp.join(""));                        
                        sol.push(format!("require(false);"));   
                        sol.push(format!("}}"));
                    }
                    may+=1;
                }
                 
            },
            Operation::If(cond,ontrue,onfalse)=>{
                let c=sol_bexpr(cond,variables,params);
                if onfalse.len()>0 {
                    sol.push(format!("if( {} ){{",c));
                }else{
                    sol.push(format!("require( {} );",c));
                }
                
                sol_operations(&ontrue,sol,variables,params,0);
                
                if onfalse.len()>0 {            
                    sol.push(format!("}}")); 
                    sol.push(format!("else{{")); 
                    sol_operations(&onfalse,sol,variables,params,0);
                    sol.push(format!("}}")); 
                }
                  
            },
            Operation::Inc(dest,e)=>{
                let dest=sol_operand(dest,variables,params);
                let e=sol_operand(e,variables,params);
                sol.push(format!("{}+={};",dest,e));  
            },
            Operation::Dec(dest,e)=>{
                let dest=sol_operand(dest,variables,params);
                let e=sol_operand(e,variables,params);
                sol.push(format!("{}-={};",dest,e));  
            },
            Operation::Sub(from,e)=>{
                let from=sol_operand(from,variables,params);
                let e=sol_operand(e,variables,params);
                sol.push(format!("{}-={};",from,e));  
            },
            Operation::Add(to,e)=>{
                let to=sol_operand(to,variables,params);
                let e=sol_operand(e,variables,params);
                sol.push(format!("{}+={};",to,e));
            },
            Operation::Call(func,cparams)=>{
                let func=sol_operand(func,variables,params);
                let cparams:Vec<String>=cparams.iter().map(|p|
                    sol_operand(p,variables,params)
                ).collect();
                sol.push(format!("{}({});",func,cparams.join(",")));  
            }
        }
    }
    if ret_list.len()>0 {
        sol.push(format!("return ({});",ret_list.join(",")));
    }
}

impl LexonVM{

    pub fn solidity(&self)->String{
        let mut sol:Vec<String>=Vec::new();
        sol.push("pragma solidity ^0.5.0;".to_string());
        for program in &self.programs{
            
            sol.push(format!("contract {}{{",program.name.replace(" ","")));
            
            for comment in &program.comments{
                sol.push(format!("/* {} */",comment.trim()));
            }
            
            for var in &program.variables{
                sol.push(format!("{} {};",type2str(&var._type),var.name.to_lowercase()));
            }
            
            let params:Vec<String>=program.constructor.parameters.iter().map(|v|  
                format!("{} {}",type2str(&v._type),v.name.to_lowercase())
            ).collect();
            
            sol.push(format!("constructor({}) public payable {{",params.join(", ")));
            
            let person= program.variables.iter().find(|x| 
                if let Type::Person=x._type {
                  x.bound
                }else{
                  false
                }
            );
            
            match person{
                Some(p)=>{
                  sol.push(format!("{}=msg.sender;",p.name));
                },
                None=>{}
            }
            
            sol_operations(&program.constructor.operations,&mut sol,&program.variables,&program.constructor.parameters,0);
            
            sol.push(format!("}}"));

            //functions

            for func in &program.terms_functions{
                let mut ret:Vec<String>=Vec::new();
                for r in &func.returns{
                    if let Some(_va) = program.variables.iter().position(
                        |x| x.name==r.name.to_lowercase() 
                    )
                    {
                        ret.push(type2str(&r._type));
                    }
                }
                let mut ret_s=String::new();
                if ret.len()>0{
                  ret_s=format!("view returns ({})", ret.join(","))
                }
                let access=(if func.is_private { "private"} else { "external" }).to_string();
                let params:Vec<String>= func.parameters.iter().map(|p|
                    //p.name.to_string()
                    format!("{} {}",type2str(&p._type),p.name.to_lowercase())
                ).collect();
                let params=params.join(",");
                sol.push(format!("function {}({}) {} {}{{",func.name,params,access,ret_s));
                
                sol_operations(&func.operations,&mut sol,&program.variables,&func.parameters,func.mays.len());
                                
                sol.push(format!("}}"));
            }
            sol.push(format!("}}"));
        }
        //println!("{:#?}",sol );
        const INDENT_WITH:usize=4;        
        let mut indent_level=0;
        let mut out:Vec<String>=Vec::new();
        for s in &sol{
            let mut sep="";
            let mut indent=format!("{:width$}","",width=indent_level*INDENT_WITH);
            if s.len()>0 {
               if s.starts_with("contract")
               || s.starts_with("constructor")
               || s.starts_with("function")
               || s.starts_with("/*") {
				sep = "\n";
		}
                let f=s.chars().next().unwrap();
                let l=s.chars().last().unwrap();
                if f!='}' && l=='{' {
                    indent_level+=1;
                }
                if f=='}' && indent_level>0{
                    indent_level-=1;
                    indent=format!("{:width$}"," ",width=indent_level*INDENT_WITH);
                }
                out.push(format!("{}{}{}",sep,indent,s));
                if f=='}' && l=='{' {
                    indent_level+=1;
                }
            }
          
          //match s.chars().last(){
          //  Some(c)=>{
          //    match c{
          //      '{'=>{
          //          indent_level+=1;
          //      },
          //      '}'=>{
          //        indent_level-=1;
          //        indent=format!("{:width$}"," ",width=indent_level*INDENT_WITH);
          //      },
          //      _=>{}
          //    }
          //  }
          //  None=>{}
          //}
          
        }
        out.join("\n")
    }

}

fn sol_varname(varname:&String)->String{
    if varname.trim().to_lowercase() == "escrow"{
        "address(this).balance".to_string()
    }else{
        varname.trim().to_lowercase()
    }
}

fn sol_expr(expr: &Expression,variables:&Vec<Variable>,params:&Vec<Variable>)->String{
    let mut output=String::new();
    let mut t=0;
    let t_ops_len=expr.ops.len();
    for term in &expr.terms{
        let mut f=0;
        let f_ops_len=term.ops.len();
        for factor in &term.factors{
            match factor{
                Factor::Sym(symbol)=>{
                    let symbol=symbol.sym.to_lowercase();
                    if variables.iter().any(|v| &v.name==&symbol ){
                      output+=&sol_varname(&symbol);                      
                    }else if params.iter().any(|v| &v.name==&symbol ){
                      output+=&sol_varname(&symbol);                      
                    }else{
                        match symbol.as_str(){
                            "true"=>{
                                output+=&format!(" {} ",symbol);
                            },
                            "false"=>{
                                output+=&format!(" {} ",symbol);
                            },
                            "escrow"=>{
                                output+=&format!(" address(this).balance ");
                            },
                            //"Amount"=>{
                            //    output+=&format!(" msg.value ");
                            //},
                            _=>{
                              //text?
                                output+=&format!(" '{}' ",symbol.to_lowercase());
                                //output+=&sol_varname(symbol);
                            }    
                        }
                    }
                    
                },
                Factor::Remainder(symbol)=>{
                  output+=&sol_varname(symbol);
                }
                Factor::Power{b,e}=>{
                    output+=&format!(" {}**{} ",b,e);
                },
                Factor::Integer(i)=>{
                    output+=&format!(" {} ",i);
                },
                Factor::Time(t)=>{
                    output+=&format!(" {} ",t);
                },
                Factor::Type(t)=>{
                    match t{
                        Type::Contract =>{
                            output+=&format!(" this ");
                        },
                        _ =>{

                        }
                    }
                }
            }
            if f < f_ops_len{
                match &term.ops[f]{
                    MultDiv::Mult=> {
                        output+=&format!(" * ");
                    },
                    MultDiv::Div=> {
                        output+=&format!(" / ");
                    },
                }
            }
            f+=1;
        }
        if t < t_ops_len{
            match &expr.ops[t]{
                PlusMinus::Plus=> {
                    output+=&format!(" + ");
                },
                PlusMinus::Minus=> {
                    output+=&format!(" - ");
                },
            }
        }
        t+=1;
    }
    output
}

fn sol_bexpr(b: &BooleanExpression,variables:&Vec<Variable>,params:&Vec<Variable>)->String{
    let mut output=String::new();
    //let mut i=0;
    let c=b.ops.len();
    //let e=b.exprs.len();
    for (i, exp) in b.exprs.iter().enumerate(){

        match exp{
            BoolStmt::Cmp{op,exp1,exp2}=>{
                let e1=sol_expr(exp1,variables,params);
                output.push_str(&e1);
                match op{
                    CmpOp::Less        =>output.push_str(" < "),
                    CmpOp::Greater     =>output.push_str(" > "),
                    CmpOp::NotEqual    =>output.push_str(" != "),
                    CmpOp::Equal       =>output.push_str(" == "),
                    CmpOp::GreaterEqual=>output.push_str(" >= "),
                    CmpOp::LessEqual   =>output.push_str(" <= ")
                }
                let e2=sol_expr(exp2,variables,params);
                output.push_str(&e2);
            },
            BoolStmt::Date(sym,cmp,time)=>{
              match cmp.as_str(){
                "isnot"=>{
                  output.push_str("!");
                },
                _=>{}
              };
              match time{
                TimeCmp::Now=>{
                  output+=&format!("({} == now )",sym.to_lowercase());
                },
                TimeCmp::Past=>{
                  output+=&format!("({} > now )",sym.to_lowercase());
                },
                TimeCmp::Future=>{
                  output+=&format!("({} > now )",sym.to_lowercase());
                },
                TimeCmp::NowOrPast=>{
                  output+=&format!("({} <= now )",sym.to_lowercase());
                },
                TimeCmp::NowOrFuture=>{
                  output+=&format!("({} >= now )",sym.to_lowercase());
                }
              }
            },
            BoolStmt::Is(sym,cmp,exp)=>{
              let ex=&sol_expr(exp,variables,params);
              match cmp.as_str(){
                "is"=>{                  
                  output+=&format!("({} == {} )",sol_varname(sym),&ex);
                },
                "isnot"=>{                  
                  output+=&format!("({} != {} )",sol_varname(sym),&ex);
                },
                _=>{}
              };              
            }
        };
        if i < c{
            match b.ops[i]{
                BoolOp::And => output.push_str(" && "),
                BoolOp::Or  => output.push_str(" || ")
            }
        }
        //i+=1
    }
    output
}
