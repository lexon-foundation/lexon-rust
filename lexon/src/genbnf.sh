#!/bin/sh
#translate to more or less ebnf
sed -E \
-e 's/^(\w+)= *[_@]?\{/<\1>:=/g' \
-e 's/\}//g' \
-e 's/\^"/"/g' \
-e 's/\s*\~\s*/></g' \
lexon.pest > lexon.bnf
