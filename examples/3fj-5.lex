LEX Paid Escrow。
LEXON: 0.2.22
COMMENT: 3.f.j-5 - an escrow that is controlled by a third party for a fee。
COMMENT: going Kanji: 5) is。

「Payer」は、a person。
「Payee」は、a person。
「Arbiter」は、a person。
「Fee」は、an amount。

The Payer pays an Amount into escrow、
appoints the Payee、
appoints the Arbiter、
and also fixes the Fee。

条項: Pay Out。
The Arbiter may pay from escrow the Fee to themselves、
and afterwards pay the remainder of the escrow to the Payee。

条項: Pay Back。
The Arbiter may pay from escrow the Fee to themselves、
and afterwards return the remainder of the escrow to the Payer。
